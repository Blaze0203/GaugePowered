//------------------------------------------------------------------------------
// Re-usable code to manage adding of email from the welcome and watchlist pages.
//------------------------------------------------------------------------------
define([
    "dojo/_base/declare",
    "dojo/_base/event",
    "dojo/_base/lang",
    "dojo/on",
    "dojo/query",
    "atemi/io/jsonrpc",
    "bootstrap/Tooltip"
], function(declare, event, lang, on, query, jsonrpc, Tooltip) {

    var rpc = jsonrpc('/jsonrpc');

    //------------------------------------------------------------------------------
    return declare(null, {

        //--------------------------------------------------------------------------
        constructor: function(options) {
            if (!options.subscribe_button_selector) {
                throw "EmailWidget.subscribe_button_selector is a required parameter";
            }
            this.subscribe_button = query(options.subscribe_button_selector);

            if (!options.email_input_selector) {
                throw "EmailWidget.email_input_selector is a required parameter";
            }
            this.email_input = query(options.email_input_selector)[0];

            if (!options.form_selector) {
                throw "EmailWidget.form_selector is a required parameter";
            }
            this.form_selector = options.form_selector;

            this.marketing_input_selector = options.marketing_input_selector;
            this.success_handler = options.success_handler;
            this.error_handler = options.error_handler;
            this.error_message = options.error_message || "Must ve a valid email address";

            // Setup the tooltip.
            this.subscribe_button.tooltip({
                trigger: 'manual',
                placement: options.error_tooltip_placement || 'right',
                html: false,
                title: lang.hitch(this, "get_error_message")
            });

            // Hide the tooltip when they are typing in the email field.
            on(this.email_input, 'focus', lang.hitch(this, "on_email_input_focus"));

            // Ensure the form is submitted the way we want
            query(this.form_selector).on("submit", lang.hitch(this, "on_form_submit"));
        },

        //--------------------------------------------------------------------------
        get_error_message: function() {
            return this.error_message;
        },

        //--------------------------------------------------------------------------
        on_form_submit: function(evt) {
            event.stop(evt);
            this.subscribe();
        },

        //--------------------------------------------------------------------------
        subscribe: function() {
            var data = {};
            data.email = this.email_input.value;
            if (this.marketing_input_selector) {
                data.marketing_notifications = query(this.marketing_input_selector)[0].checked;
            }

            rpc.request({
                    method: 'useraccountemail.create',
                    params: ['select(id)', data]
                },
                lang.hitch(this, "on_subscribe_success"),
                lang.hitch(this, "on_subscribe_failure")
            );
        },

        //--------------------------------------------------------------------------
        on_subscribe_success: function(data) {
            this.subscribe_button.tooltip('hide');
            this.email_input.value = "";
            if (this.success_handler) {
                return this.success_handler(data);
            }
            return false;
        },

        //--------------------------------------------------------------------------
        on_subscribe_failure: function(error) {
            message  = error.error.message;

            if (message.indexOf("Refusing to email them") != -1) {
                this.error_message = '"' + email + '" cannot be added to your account.';

            } else if (message.indexOf("is not a valid e-mail address") != -1) {
                this.error_message = '"' + email + '" is not a valid email address.';

            } else {
                this.error_message = "There was a problem adding this email address.";
            }
            this.subscribe_button.tooltip('show');
            if (this.error_handler) {
                return this.error_handler(error);
            }
            return false;
        },

        //--------------------------------------------------------------------------
        on_email_input_focus: function(evt) {
            this.subscribe_button.tooltip('hide');
        }
    });
});
