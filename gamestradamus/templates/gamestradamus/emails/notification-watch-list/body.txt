Hey {{ user_account.nickname }},

You've been watching {{ game.name }} and wanted to know when the {{ watch.operation }} dropped below {% if watch.operation == "price" %}${{ watch.limit }}{% else %}${{ watch.limit }} / hour{% endif %}. Well guess what, it just did!

Check out the details here: http://www.gaugepowered.com/game/{{ game.id }}/{# &cc={{ region }} #}

Gauge


Don't want notifications for this game anymore - change your watchlist details here: http://www.gaugepowered.com/profile/watchlist/
