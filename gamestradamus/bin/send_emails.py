"""A cronable script that will send out all emails waiting for sending.

Uses a tablewide lock to ensure that we're not processing the queue more than
once.
"""
from jamaisvu import pglock

from plumbing.email import units
from plumbing.email.backends import mandrill

from gamestradamus import cli

#-------------------------------------------------------------------------------
if __name__ == "__main__":
    store = cli.storage_manager()
    sandbox = store.new_sandbox()
    config = cli.get_global_config()
    api_key = config['gauge.mandrill.api_key']

    with pglock.PGAdvisoryLock(sandbox, "GAUGE-EMAIL-QUEUE"):
        emails = units.Email.email_queue(sandbox)
        mandrill.process_queue(api_key, emails)
    sandbox.flush_all()
    store.shutdown()

