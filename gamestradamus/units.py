
from copy import copy
import cherrypy
import datetime
import decimal
import dejavu
import jamaisvu
import plumbing.auth
from cream import rpc
from gamestradamus import timeutils
from gamestradamus.thirdparty import crypto
from gamestradamus.regions import REGIONS
from jamaisvu.pglock import PGAdvisoryLock

__all__ = [
    "Achievement",
    "Game",
    "Genre",
    "GameGenre",
    "PossibleDuplicate",
    "StatSet",
    'UserAccountAchievement',
    'UserAccountEmail',
    "UserAccountGame",
    "UserAccountGameWatch",
    "UserAccountPayment",
    "UserAccountPrediction",
    "UserAccountRelative",
    "UserAccountStatistics",
    "UserAccountAttributes",
    "UserAccountTag",
    "UserAccountFriend",
    "UserAccountInvitation",
]

#-------------------------------------------------------------------------------
TIMEZONE_AWARE = {'timezone_aware': True}

#-------------------------------------------------------------------------------
ZERO = decimal.Decimal('0.00')


#-------------------------------------------------------------------------------
class StatSet(rpc.dejavu.Unit):
    """Games that are part of the same Stats Set have the same stats, Duh!

    This identity record simple exists to allow us to map a set of games as
    being part of the same set.
    """
    # Update our id to only be 2 bytes large, which allows 32767 ids.
    id = dejavu.UnitProperty(int, index=True, hints={'bytes': 2})

#---------------------------------------------------------------------------
def normalize_genres(sandbox, game_id, genre):
    """Ensures we have the necessary GameGenre relationships.

    Using our genres field, ensure that the appropriate Genre's exist and
    that this game is related to them.
    """
    if genre is None:
        return

    with PGAdvisoryLock(sandbox, "%s-genres" % game_id):
        # Get a list of the genres we SHOULD be associating with it.
        genre_titles = list(set([g.strip() for g in genre.split(",")]))
        genres = []
        for genre_title in genre_titles:
            genre, _ = Genre.get_or_create(sandbox, genre_title)
            genres.append(genre)

        # Now get all the ones already associated with it.
        game_genres = sandbox.xrecall(GameGenre, lambda gg: gg.game_id == game_id)
        for game_genre in game_genres:
            sandbox.forget(game_genre)

        # Now add in the new ones
        for genre in genres:
            game_genre = GameGenre(game_id=game_id, genre_id=genre.id)
            sandbox.memorize(game_genre)

#-------------------------------------------------------------------------------
class Game(rpc.dejavu.Unit):
    """A single game."""

    # Update our id to only be 2 bytes large, which allows 32767 ids.
    id = dejavu.UnitProperty(int, index=True, hints={'bytes': 2})

    app_id = dejavu.UnitProperty(int, index=True, hints={'bytes': 4})
    predictable_cost = dejavu.UnitProperty(bool, default=True)
    is_dlc = dejavu.UnitProperty(bool, index=True, default=False)

    # Canonical game is used within a particular region to indicate that
    # we should only display and link people to the canonical game within
    # that region. 
    canonical_game_id = dejavu.UnitProperty(int, hints={'bytes': 2})

    # Stat Set groups games into the same statistical sets so that we can
    # ensure that the games with legitimately different records between 
    # regions still pull and share the same statistical information.
    stat_set_id = dejavu.UnitProperty(int, index=True, hints={'bytes': 2})

    name = dejavu.UnitProperty(unicode, index=True)
    genre = dejavu.UnitProperty(unicode)
    release = dejavu.UnitProperty(datetime.datetime)
    logo = dejavu.UnitProperty(unicode)

    us_price = dejavu.UnitProperty(decimal.Decimal)
    us_sale_price = dejavu.UnitProperty(decimal.Decimal)
    us_on_sale_since = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)
    us_available_since = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)

    uk_price = dejavu.UnitProperty(decimal.Decimal)
    uk_sale_price = dejavu.UnitProperty(decimal.Decimal)
    uk_on_sale_since = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)
    uk_available_since = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)

    eu_price = dejavu.UnitProperty(decimal.Decimal)
    eu_sale_price = dejavu.UnitProperty(decimal.Decimal)
    eu_on_sale_since = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)
    eu_available_since = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)

    eu2_price = dejavu.UnitProperty(decimal.Decimal)
    eu2_sale_price = dejavu.UnitProperty(decimal.Decimal)
    eu2_on_sale_since = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)
    eu2_available_since = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)

    de_price = dejavu.UnitProperty(decimal.Decimal)
    de_sale_price = dejavu.UnitProperty(decimal.Decimal)
    de_on_sale_since = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)
    de_available_since = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)

    au_price = dejavu.UnitProperty(decimal.Decimal)
    au_sale_price = dejavu.UnitProperty(decimal.Decimal)
    au_on_sale_since = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)
    au_available_since = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)

    jp_price = dejavu.UnitProperty(decimal.Decimal)
    jp_sale_price = dejavu.UnitProperty(decimal.Decimal)
    jp_on_sale_since = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)
    jp_available_since = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)

    mx_price = dejavu.UnitProperty(decimal.Decimal)
    mx_sale_price = dejavu.UnitProperty(decimal.Decimal)
    mx_on_sale_since = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)
    mx_available_since = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)

    ua_price = dejavu.UnitProperty(decimal.Decimal)
    ua_sale_price = dejavu.UnitProperty(decimal.Decimal)
    ua_on_sale_since = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)
    ua_available_since = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)

    ru_price = dejavu.UnitProperty(decimal.Decimal)
    ru_sale_price = dejavu.UnitProperty(decimal.Decimal)
    ru_on_sale_since = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)
    ru_available_since = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)

    br_price = dejavu.UnitProperty(decimal.Decimal)
    br_sale_price = dejavu.UnitProperty(decimal.Decimal)
    br_on_sale_since = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)
    br_available_since = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)

    average_hours_played = dejavu.UnitProperty(decimal.Decimal)
    median_hours_played = dejavu.UnitProperty(decimal.Decimal)
    owner_count = dejavu.UnitProperty(int, hints={'bytes': 4})

    rating = dejavu.UnitProperty(decimal.Decimal)
    rating_count = dejavu.UnitProperty(int, hints={'bytes': 4})

    hours_played_distribution = dejavu.UnitProperty(unicode)

    #---------------------------------------------------------------------------
    def get_region_attr(self, attrs, region):
        """Choose the appropriate value based on the region.

        Assumes that the dictionary always contains a 'us' key which will
        be the default of the user doesn't have a region or they are from
        a region where we don't have a value.
        """
        default_region = 'us'
        if not region:
            user_region = getattr(cherrypy.request, 'user_region', None)
            if not user_region:
                region = default_region
            else:
                region = user_region['value']

        return attrs.get(region)

    #---------------------------------------------------------------------------
    def get_price(self, region=None):
        return self.get_region_attr(dict([
            (key, getattr(self, settings['game_attributes']['price']),)
            for key, settings in REGIONS.iteritems()
        ]), region)
    price = property(get_price)

    #---------------------------------------------------------------------------
    def get_sale_price(self, region=None):
        return self.get_region_attr(dict([
            (key, getattr(self, settings['game_attributes']['sale_price']),)
            for key, settings in REGIONS.iteritems()
        ]), region)
    sale_price = property(get_sale_price)

    #---------------------------------------------------------------------------
    def get_on_sale_since(self, region=None):
        return self.get_region_attr(dict([
            (key, getattr(self, settings['game_attributes']['on_sale_since']),)
            for key, settings in REGIONS.iteritems()
        ]), region)
    on_sale_since = property(get_on_sale_since)

    #---------------------------------------------------------------------------
    def get_min_price(self, region=None):
        return min(
            self.get_price(region) or ZERO,
            self.get_sale_price(region) or ZERO,
        )
    min_price = property(get_min_price)

    #---------------------------------------------------------------------------
    def get_community_value(self, region=None):
        if self.median_hours_played:
            return min(
                (self.get_min_price(region) / self.median_hours_played).quantize(decimal.Decimal('0.00')),
                self.get_min_price(region)
            )
        else:
            return self.get_min_price(region)
    community_value = property(get_community_value)

    #---------------------------------------------------------------------------
    @property
    def community_rating(self):
        if self.rating_count:
            return self.rating / self.rating_count
        else:
            return ZERO

    #---------------------------------------------------------------------------
    @property
    def community_value_base_price(self):
        if self.median_hours_played:
            return min(
                ((self.price or ZERO) / self.median_hours_played).quantize(decimal.Decimal('0.00')),
                (self.price or ZERO)
            )
        else:
            return self.price or ZERO

    #---------------------------------------------------------------------------
    @property
    def percent_off(self):
        if self.price:
            return ((self.price - self.sale_price) / self.price) * 100
        else:
            return ZERO

    #---------------------------------------------------------------------------
    def get_is_on_sale(self):
        return self.on_sale_since is not None
    on_sale = property(get_is_on_sale)

    #---------------------------------------------------------------------------
    def get_canonical_game(self):
        if self.canonical_game_id:
            return self.Game()
        else:
            return self
    canonical_game = property(get_canonical_game)

    #---------------------------------------------------------------------------
    def normalize_genres(self):
        """Ensures we have the necessary GameGenre relationships.

        Using our genres field, ensure that the appropriate Genre's exist and
        that this game is related to them.
        """
        normalize_genres(self.sandbox, self.id, self.genre)

Game.one_to_many('id', Game, 'canonical_game_id')
Game.one_to_many('id', StatSet, 'stat_set_id')


#-------------------------------------------------------------------------------
class Genre(rpc.dejavu.Unit):
    """Games can have many genres."""

    # We will probably not have more than 32k genres.
    id = dejavu.UnitProperty(int, index=True, hints={'bytes': 2})

    name = dejavu.UnitProperty(unicode)

    #---------------------------------------------------------------------------
    @classmethod
    def get_or_create(cls, sandbox, name):
        with PGAdvisoryLock(sandbox, "%s-genre"):
            created = False
            genre = sandbox.unit(Genre, name=name)
            if not genre:
                genre = Genre(name=name)
                sandbox.memorize(genre)
                created = True
        return genre, created

#-------------------------------------------------------------------------------
class GameGenre(rpc.dejavu.Unit):
    """Games can have many genres."""

    game_id = dejavu.UnitProperty(int, hints={'bytes': 2})
    genre_id = dejavu.UnitProperty(int, hints={'bytes': 2})

GameGenre.many_to_one('game_id', Game, 'id')
GameGenre.many_to_one('genre_id', Genre, 'id')

#-------------------------------------------------------------------------------
class Achievement(rpc.dejavu.Unit):
    """An achievement that a user can accomplish for a game."""

    game_id = dejavu.UnitProperty(int, index=True)
    name = dejavu.UnitProperty(unicode, index=True)
    description = dejavu.UnitProperty(unicode)

Achievement.many_to_one('game_id', Game, 'id')

#-------------------------------------------------------------------------------
class UserAccountGame(rpc.dejavu.Unit):
    """A game that a UserAccount owns."""

    user_account_id = dejavu.UnitProperty(int, hints={'bytes': 4})
    game_id = dejavu.UnitProperty(int, index=True, hints={'bytes': 2})

    user_account_id_game_id = dejavu.UnitIndex('user_account_id', 'game_id')

    steam_hours_played = dejavu.UnitProperty(decimal.Decimal)
    custom_hours_played = dejavu.UnitProperty(decimal.Decimal)

    # The dollars_paid is populated from the last known price when a game is
    # added to a library. If the user updates the data, it is marked as being corrected.
    dollars_paid = dejavu.UnitProperty(decimal.Decimal)
    corrected = dejavu.UnitProperty(bool, default=False)
    finished = dejavu.UnitProperty(bool, default=False)
    tags = dejavu.UnitProperty(unicode)

    cost_per_hour = dejavu.UnitProperty(decimal.Decimal)
    rating = dejavu.UnitProperty(int)
    ignore = dejavu.UnitProperty(bool)

    added_by_user = dejavu.UnitProperty(bool, default=False)

    def update_cost_per_hour(self):
        if (
            (self.dollars_paid or 0) > 0 and
            ((self.steam_hours_played or 0) + (self.custom_hours_played or 0)) > 0
        ):
            self.cost_per_hour = (
                self.dollars_paid / ((self.steam_hours_played or 0) + (self.custom_hours_played or 0))
            )
            # Upper bound the cost per hour to the amount paid.
            if self.cost_per_hour > self.dollars_paid:
                self.cost_per_hour = self.dollars_paid
        else:
            self.cost_per_hour = '0.0'

    #---------------------------------------------------------------------------
    @property
    def hours_played(self):
        zero = decimal.Decimal('0.0')
        return (
                self.steam_hours_played or zero
            ) + (
                self.custom_hours_played or zero
            )

    #---------------------------------------------------------------------------
    def set_initial_price(self, region, game=None):
        if game is None:
            game = self.Game()
        # Update the dollars paid if the record hasn't been corrected.
        if not self.corrected:
            self.dollars_paid = (game.get_price(region) or '0.00') if not game.canonical_game_id else (game.Game().get_price(region) or '0.00')

    #---------------------------------------------------------------------------
    def to_dict(self):
        props = copy(self._properties)

        if props['steam_hours_played'] is not None:
            props['steam_hours_played'] = str(props['steam_hours_played'].quantize(decimal.Decimal('0.0')))
        else:
            props['steam_hours_played'] = '0.0'

        if props['custom_hours_played'] is not None:
            props['custom_hours_played'] = str(props['custom_hours_played'].quantize(decimal.Decimal('0.0')))
        else:
            props['custom_hours_played'] = '0.0'

        if props['cost_per_hour'] is not None:
            props['cost_per_hour'] = str(props['cost_per_hour'].quantize(decimal.Decimal('0.00')))
        else:
            props['cost_per_hour'] = '0.00'

        if props['dollars_paid'] is not None:
            props['dollars_paid'] = str(props['dollars_paid'].quantize(decimal.Decimal('0.00')))
        else:
            props['dollars_paid'] = '0.00'

        if props['ignore'] is None:
            props['ignore'] = False

        if props['rating'] is None:
            props['rating'] = 0

        return props

UserAccountGame.many_to_one('game_id', Game, 'id')
UserAccountGame.many_to_one('user_account_id', plumbing.auth.units.UserAccount, 'id')

#-------------------------------------------------------------------------------
class UserAccountAchievement(rpc.dejavu.Unit):
    """An achievement held by a user account for a specific game"""

    # The UserAccountAchievement id is probably the only id we need
    # to be significantly large, because it stores: 
    # (Users) * (Games) * (Achievements per game)
    id = dejavu.UnitProperty(int, index=True, hints={'bytes': 8})

    user_account_id = dejavu.UnitProperty(int, hints={'bytes': 4}, index=True)
    achievement_id = dejavu.UnitProperty(int, index=True)

UserAccountAchievement.many_to_one('achievement_id', Achievement, 'id')
UserAccountAchievement.many_to_one('user_account_id', plumbing.auth.units.UserAccount, 'id')

#-------------------------------------------------------------------------------
class UserAccountTag(rpc.dejavu.Unit):
    """A set of tags that are managed by a user that can be applied to their games."""

    user_account_id = dejavu.UnitProperty(int, hints={'bytes': 4}, index=True)
    name = dejavu.UnitProperty(unicode)
    group = dejavu.UnitProperty(unicode)

    #---------------------------------------------------------------------------
    @property
    def count(self):
        items = self.sandbox.store.view((
            UserAccountGame,
            lambda x: (x.tags,),
            lambda x: x.user_account_id == self.user_account_id
        ))
        items = [x for x in items if (x[0] and self.qualified_name in x[0].split('|'))]
        return len(items)

    #---------------------------------------------------------------------------
    @property
    def qualified_name(self):
        if self.group:
            return ' / '.join([self.group, self.name])
        else:
            return self.name

UserAccountTag.many_to_one('user_account_id', plumbing.auth.units.UserAccount, 'id')

#-------------------------------------------------------------------------------
class UserAccountFriend(rpc.dejavu.Unit):
    """
    A Friend relationship between two UserAccounts.

    Two accounts that are friends can see each other's Libraries. For each
    Friend relationship there will be two records for query performance.

    A (user_account_id) -> B (friend_id)
    B (user_account_id) -> A (friend_id)
    """

    user_account_id = dejavu.UnitProperty(int, hints={'bytes': 4}, index=True)
    friend_id = dejavu.UnitProperty(int, hints={'bytes': 4})
    since = dejavu.UnitProperty(
        datetime.datetime, hints=TIMEZONE_AWARE
    )

# Be careful with this relationship. We need dejavu to return the friend UserAccount
# as a default join operation.
UserAccountFriend.many_to_one('friend_id', plumbing.auth.units.UserAccount, 'id')

#-------------------------------------------------------------------------------
class UserAccountInvitation(rpc.dejavu.Unit):
    """
    An invitation for another UserAccount to become friends.
    """
    user_account_id = dejavu.UnitProperty(int, hints={'bytes': 4}, index=True)
    friend_id = dejavu.UnitProperty(int, hints={'bytes': 4})
    email = dejavu.UnitProperty(unicode)
    invite_code = dejavu.UnitProperty(unicode, index=True)
    created = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)

    #---------------------------------------------------------------------------
    def set_defaults(self, code_size):
        self.invite_code = crypto.get_random_string(code_size)
        self.created = timeutils.utcnow()

    #---------------------------------------------------------------------------
    def accept_link(self):
        return "http://www.gaugepowered.com/ai/%s" % self.invite_code

    #---------------------------------------------------------------------------
    def unsubscribe_link(self):
        return 'http://www.gaugepowered.com'

UserAccountInvitation.many_to_one('user_account_id', plumbing.auth.units.UserAccount, 'id')

#-------------------------------------------------------------------------------
class UserAccountRelative(rpc.dejavu.Unit):
    """A relative to a UserAccount."""

    user_account_id = dejavu.UnitProperty(int, hints={'bytes': 4}, index=True)
    relative_id = dejavu.UnitProperty(int)
    match_score = dejavu.UnitProperty(decimal.Decimal)
    similar_count = dejavu.UnitProperty(int, hints={'bytes': 4})

UserAccountRelative.many_to_one('relative_id', plumbing.auth.units.UserAccount, 'id')

#-------------------------------------------------------------------------------
class UserAccountPrediction(jamaisvu.Unit):
    """A UserAccount Prediction."""

    user_account_id = dejavu.UnitProperty(int, hints={'bytes': 4}, index=True)

    game_id = dejavu.UnitProperty(int, hints={'bytes': 2})
    hours_played = dejavu.UnitProperty(decimal.Decimal)

UserAccountPrediction.many_to_one('user_account_id', plumbing.auth.units.UserAccount, 'id')
UserAccountPrediction.many_to_one('game_id', Game, 'id')

#-------------------------------------------------------------------------------
class UserAccountStatistics(jamaisvu.Unit):
    """A set of global statistics for a UserAccount."""

    user_account_id = dejavu.UnitProperty(int, hints={'bytes': 4}, index=True)

    game_count = dejavu.UnitProperty(int, hints={'bytes': 4})
    hours_played = dejavu.UnitProperty(decimal.Decimal)
    achievement_count = dejavu.UnitProperty(int, hints={'bytes': 4})

UserAccountStatistics.one_to_one('user_account_id', plumbing.auth.units.UserAccount, 'id')

#-------------------------------------------------------------------------------
class UserAccountAttributes(jamaisvu.Unit):
    """Tags that are assigned to a UserAccount"""

    user_account_id = dejavu.UnitProperty(int, hints={'bytes': 4}, index=True)

    premium_account_until = dejavu.UnitProperty(
        datetime.datetime, hints=TIMEZONE_AWARE
    )

    # Store whether they are a private account
    # when we last checked whether they were private or not.
    private_profile_since = dejavu.UnitProperty(
        datetime.datetime, hints=TIMEZONE_AWARE
    )

    welcome_started = dejavu.UnitProperty(
        datetime.datetime, hints=TIMEZONE_AWARE
    )
    welcomed_since = dejavu.UnitProperty(
        datetime.datetime, hints=TIMEZONE_AWARE
    )
    agree_to_terms_since = dejavu.UnitProperty(
        datetime.datetime, hints=TIMEZONE_AWARE
    )

    games_updating = dejavu.UnitProperty(bool)
    games_last_updated = dejavu.UnitProperty(
        datetime.datetime, hints=TIMEZONE_AWARE
    )

    predictions_updating = dejavu.UnitProperty(bool)
    relatives_last_updated = dejavu.UnitProperty(
        datetime.datetime, hints=TIMEZONE_AWARE
    )
    predictions_last_updated = dejavu.UnitProperty(
        datetime.datetime, hints=TIMEZONE_AWARE
    )

    data_usage_opt_out = dejavu.UnitProperty(
        datetime.datetime, hints=TIMEZONE_AWARE
    )

    has_ignored_watchlist_email = dejavu.UnitProperty(bool)

    region = dejavu.UnitProperty(unicode, default="us")

    #---------------------------------------------------------------------------
    def get_is_premium_account(self):
        if self.premium_account_until:
            return timeutils.utcnow() <= self.premium_account_until
        else:
            return False
    premium_account = property(get_is_premium_account)

    #---------------------------------------------------------------------------
    def get_is_welcomed(self):
        return self.welcomed_since is not None
    welcomed = property(get_is_welcomed)

    #---------------------------------------------------------------------------
    def to_dict(self):
        props = copy(self._properties)
        names_of_properties_that_are_dates = [
            'private_profile_since',
            'welcome_started',
            'welcomed_since',
            'agree_to_terms_since',
            'games_last_updated',
            'relatives_last_updated',
            'predictions_last_updated',
            'data_usage_opt_out',
        ]
        for property_name in names_of_properties_that_are_dates:
            if props[property_name] is not None:
                props[property_name] = props[property_name].isoformat()

        return props

UserAccountAttributes.one_to_one('user_account_id', plumbing.auth.units.UserAccount, 'id')

#-------------------------------------------------------------------------------
class UserAccountPayment(jamaisvu.Unit):
    """Represents a payment for additional services.

    Currently we are accepting pay what you want.
    """

    user_account_id = dejavu.UnitProperty(int, hints={'bytes': 4}, index=True)
    order_id = dejavu.UnitProperty(unicode)
    what = dejavu.UnitProperty(unicode)
    when = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)
    amount = dejavu.UnitProperty(decimal.Decimal)

UserAccountPayment.many_to_one('user_account_id', plumbing.auth.units.UserAccount, 'id')

#-------------------------------------------------------------------------------
class UserAccountGameWatch(rpc.dejavu.Unit):
    """A game that a UserAccount is watching for a specific cost_per_hour or cost."""

    user_account_id = dejavu.UnitProperty(int, hints={'bytes': 4}, index=True)
    game_id = dejavu.UnitProperty(int, index=True, hints={'bytes': 2})

    operation = dejavu.UnitProperty(unicode)
    limit = dejavu.UnitProperty(decimal.Decimal)

    #---------------------------------------------------------------------------
    _operations = {
        "price": lambda s, g, r: g.get_min_price(r) <= s.limit,
        "cost-per-hour": lambda s, g, r: g.get_community_value(r) <= s.limit,
    }
    #---------------------------------------------------------------------------
    def satisfied(self, game=None, region=None):
        if game is None:
            game = self.Game()
        if not game:
            return False
        if not self.limit:
            return False

        operation = self._operations.get(self.operation)
        if not operation:
            return False

        if not region:
            user_attrs = self.UserAccount().UserAccountAttributes()
            region = "us"
            if user_attrs:
                region = user_attrs.region

        return operation(self, game, region)

    #---------------------------------------------------------------------------
    def to_dict(self):
        props = copy(self._properties)

        if props['limit'] is not None:
            props['limit'] = str(props['limit'].quantize(decimal.Decimal('0.00')))

        return props

UserAccountGameWatch.many_to_one('game_id', Game, 'id')
UserAccountGameWatch.many_to_one('user_account_id', plumbing.auth.units.UserAccount, 'id')

#-------------------------------------------------------------------------------
class UserAccountEmail(rpc.dejavu.Unit):
    """Information about an email address."""

    user_account_id = dejavu.UnitProperty(int, hints={'bytes': 4}, index=True)
    email = dejavu.UnitProperty(unicode)

    confirmation_code = dejavu.UnitProperty(unicode)
    date_confirmed = dejavu.UnitProperty(datetime.datetime, hints=TIMEZONE_AWARE)

    watch_list_notifications = dejavu.UnitProperty(bool, default=True, index=True)
    watch_list_unsubscribe_code = dejavu.UnitProperty(unicode)

    marketing_notifications = dejavu.UnitProperty(bool, default=True, index=True)
    marketing_unsubscribe_code = dejavu.UnitProperty(unicode)

    permanent_unsubscribe_code = dejavu.UnitProperty(unicode)

    #---------------------------------------------------------------------------
    def set_defaults(self, code_size):
        self.watch_list_notifications = True
        if self.marketing_notifications is None:
            self.marketing_notifications = True
        self.confirmation_code = crypto.get_random_string(code_size)
        self.date_confirmed = None
        self.watch_list_unsubscribe_code = crypto.get_random_string(code_size)
        self.marketing_unsubscribe_code = crypto.get_random_string(code_size)
        self.permanent_unsubscribe_code = crypto.get_random_string(code_size)

    #---------------------------------------------------------------------------
    def confirmation_link(self):
        return "http://www.gaugepowered.com/ce/%s" % self.confirmation_code

    #---------------------------------------------------------------------------
    def unsubscribe_link(self):
        return "http://www.gaugepowered.com/unsubscribe/%s" % self.permanent_unsubscribe_code

    #---------------------------------------------------------------------------
    def to_dict(self):
        props = copy(self._properties)

        if props['date_confirmed'] is not None:
            props['date_confirmed'] = props['date_confirmed'].isoformat()

        return props

UserAccountEmail.many_to_one('user_account_id', plumbing.auth.units.UserAccount, 'id')

#-------------------------------------------------------------------------------
class PossibleDuplicate(rpc.dejavu.Unit):
    """Stores the list of games that are potential duplicates.


    Our algorithm for identifying potential duplicats is pretty naive,
    so we frequently need to mark games as not being name duplicates.
    If the false_positive field is set this this is a false name positive -
    and should not be considered duplicate for any reason.

    Example: Prototype vs Prototype 2

    Each pair in here should be unique - and if they are in here our duplicate
    finder will ignore them.
    """
    game_id_1 = dejavu.UnitProperty(int, index=True, hints={'bytes': 2})
    game_id_2 = dejavu.UnitProperty(int, index=True, hints={'bytes': 2})

    # Resolved will be set to true when this has been resolved.
    resolved = dejavu.UnitProperty(bool)

    #---------------------------------------------------------------------------
    @classmethod
    def is_name_duplicate(cls, sandbox, game_id_1, game_id_2):
        unit = sandbox.recall(cls, lambda game: (
            (game.game_id_1 == game_id_1 and game.game_id_2 == game_id_2) or
            (game.game_id_1 == game_id_2 and game.game_id_2 == game_id_1)
        ))
        if not unit:
            return False
        return True

    #---------------------------------------------------------------------------
    def game_1(self):
        return self.sandbox.unit(Game, id=self.game_id_1)

    #---------------------------------------------------------------------------
    def game_2(self):
        return self.sandbox.unit(Game, id=self.game_id_2)
PossibleDuplicate.many_to_one('game_1_id', Game, 'id')
PossibleDuplicate.many_to_one('game_2_id', Game, 'id')


#-------------------------------------------------------------------------------
def __register__():
    return {
        'Achievement': Achievement,
        'Game': Game,
        'GameGenre': GameGenre,
        'Genre': Genre,
        'PossibleDuplicate': PossibleDuplicate,
        'UserAccountAchievement': UserAccountAchievement,
        'UserAccountEmail': UserAccountEmail,
        'UserAccountGame': UserAccountGame,
        'UserAccountGameWatch': UserAccountGameWatch,
        'UserAccountPayment': UserAccountPayment,
        'UserAccountPrediction': UserAccountPrediction,
        'UserAccountRelative': UserAccountRelative,
        'UserAccountStatistics': UserAccountStatistics,
        'UserAccountAttributes': UserAccountAttributes,
        'UserAccountTag': UserAccountTag,
        'UserAccountFriend': UserAccountFriend,
        'UserAccountInvitation': UserAccountInvitation,
        'StatSet': StatSet,
    }
